/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   ADC.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro \n B13061
\version
\date
\warning None
* History:
*/

#ifndef _ADC_H
#define _ADC_H


#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"
#include "PublicVariables.h"


/* Defines */
#define ADC_CH 6  /* Number of ADC channels used for Knobs value reading */


/* Declarations */


/* Function Prototypes */
void vfnADCSetup(void);
void vfnReadADCValues(void);
void vfnReadADCH(UINT8);


#endif /* _ADC_H */