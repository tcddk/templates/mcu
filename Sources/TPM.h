/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   TPM.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro \n B13061
\version
\date
\warning None
* History:
*/

#ifndef _TPM_H
#define _TPM_H


#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"

/* Defines */

#define LED_PWM_PERIOD		50000
#define LED_RED_PWM			TPM2C1V
#define LED_GREEN_PWM		TPM2C0V


/* Function Prototypes */

void vfnTPM1Setup(void);
void vfnTPM2Setup(void);


#endif /* _TPM_H */