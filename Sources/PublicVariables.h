/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   PublicVariables.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Samuel Quiroz \n B05113
\version
\date
\warning None
* History:
*/
#ifndef _PUBLIC_VAR_H
#define _PUBLIC_VAR_H


#include "VarTemplate.h"
#include "typedef.h"



/* Defines */

#define RAM_START_ADDR   0xBD   //Don't modify this value, it's used by the USB handler



/* User can use this macros to access the public variables */
#define		KNOB_1	              gua8Knobs[0]
#define		KNOB_2	              gua8Knobs[1]
#define		KNOB_3	              gua8Knobs[2]
#define		KNOB_4       	        gua8Knobs[3]
#define		KNOB_5	              gua8Knobs[4]
#define		KNOB_6	              gua8Knobs[5]
#define		SWITCH_1	            gua8Switches[0]
#define		SWITCH_2              gua8Switches[1]
#define		FOOT_SWITCH_TAP      	gua8Switches[2]
#define		FOOT_SWITCH	          gua8Switches[3]
#define		RED_LED	              gua8LEDs[0]
#define		RED_LED_PWM_VAL        gua16LEDsPWM[0]
#define		GREEN_LED	            gua8LEDs[1]
#define		GREEN_LED_PWM_VAL       gua16LEDsPWM[1]

/* The only way to access this variables is by accessing each bytes separately */
#define		DEBUG_VAR_FROM_DSP_1	gua24DebugDataFromDSP[0]
#define		DEBUG_VAR_FROM_DSP_2	gua24DebugDataFromDSP[1]
#define		DEBUG_VAR_FROM_DSP_3	gua24DebugDataFromDSP[2]
#define		DEBUG_VAR_FROM_DSP_4	gua24DebugDataFromDSP[3]
#define		DEBUG_VAR_TO_DSP_1	  gua24DebugDataToDSP[0]
#define		DEBUG_VAR_TO_DSP_2	  gua24DebugDataToDSP[1]
#define		DEBUG_VAR_TO_DSP_3	  gua24DebugDataToDSP[2]
#define		DEBUG_VAR_TO_DSP_4	  gua24DebugDataToDSP[3]


/* These are the addresses where the DSP stores the variables */
#define		KNOB_1_ADDR_DSP							0x00
#define		KNOB_2_ADDR_DSP							0x01
#define		KNOB_3_ADDR_DSP							0x02
#define		KNOB_4_ADDR_DSP							0x03
#define		KNOB_5_ADDR_DSP							0x04
#define		KNOB_6_ADDR_DSP							0x05

#define		SWITCH_1_ADDR_DSP							0x06
#define		SWITCH_2_ADDR_DSP							0x07
#define		FOOT_SWITCH_TAP_ADDR_DSP				0x08
#define		FOOT_SWITCH_ADDR_DSP						0x09

#define		RED_LED_ADDR_DSP							0x0A
#define		RED_LED_PWM_ADDR_DSP						0x0B
#define		GREEN_LED_ADDR_DSP						0x0C
#define		GREEN_LED_PWM_ADDR_DSP					0x0D

#define		DEBUG_VAR_TO_DSP_1_ADDR_DSP			0x0E
#define		DEBUG_VAR_TO_DSP_2_ADDR_DSP			0x0F
#define		DEBUG_VAR_TO_DSP_3_ADDR_DSP			0x10
#define		DEBUG_VAR_TO_DSP_4_ADDR_DSP			0x11

#define		DEBUG_VAR_FROM_DSP_1_ADDR_DSP			0x12
#define		DEBUG_VAR_FROM_DSP_2_ADDR_DSP			0x13
#define		DEBUG_VAR_FROM_DSP_3_ADDR_DSP			0x14
#define		DEBUG_VAR_FROM_DSP_4_ADDR_DSP			0x15

                                                
/* These are the commands for the DSP communication */
#define   CMD_READ_FROM_DSP            0x08
#define   CMD_WRITE_TO_DSP             0x01


/* These are the bits for the gu8DSP_Flags */
#define DebugDataFromPC                Bits.DebugDataFromPC
#define ReadyForBoot                   Bits.ReadyForBoot
#define SendDataToDSP                  Bits.SendDataToDSP
#define DataSent                       Bits.DataSent
 

/* Declarations */
extern UINT8 gua8PublicVariables[];

extern UINT8 gua8Knobs[];
extern UINT8 gua8Switches[];
extern UINT8 gua8LEDs[];
extern UINT16 gua16LEDsPWM[];
extern t_UINT24 gua24DebugDataFromDSP[];
extern t_UINT24 gua24DebugDataToDSP[];

extern u8DebugFlag gu8DSP_Flags;
extern const UINT8 gu8ReadyForBootloadDSP;


/* Function Prototypes */



#endif /* PUBLIC_VAR_H */