/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	TPM.c
  \brief    	
  \author   	Freescale Semiconductor
  \author     Diego Haro \n B13061
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    None

  * History:
  
*/

#include "TPM.h" 


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   TPM1 Module Initialization, DSP CLK configuration @ 3.000 MHz 
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  

void vfnTPM1Setup(void)
{
  TPM1C2SC = TPM1C0SC_MS0B_MASK | TPM1C0SC_ELS0A_MASK;
  TPM1C2V = 1;
  
  TPM1MOD = 1;
  TPM1SC = TPM1SC_CLKSA_MASK|TPM1SC_PS1_MASK;
}

void vfnTPM2Setup(void)
{
  TPM2C0SC = 0x28;
  TPM2C0V = 0;
  
  TPM2C1SC = 0x28;
  TPM2C1V = 0;
  
  TPM2MOD = LED_PWM_PERIOD - 1;
  TPM2SC = 0x0A;
}
