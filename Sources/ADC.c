/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	ADC.c
  \brief    	Driver to Configure and Read Values of the selected ADC Channels 
  \author   	Freescale Semiconductor
  \author     Diego Haro \n B13061
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    None

  * History:
  
*/

#include "ADC.h" /* include macros & defines declarations */

UINT8 gu8adc_buf[12];
///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   ADC Module Initialization, 8 bit resolution, 6 channels enabled for Knobs reading
            and 1 channel for Battery Level reading
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  
void vfnADCSetup(void)
{
    ADCCFG = 0x00;
    ADCSC2 = 0x00;
    ADCSC1 = 0x00;
    APCTL1 = APCTL1_ADPC5_MASK | APCTL1_ADPC4_MASK | APCTL1_ADPC3_MASK |   /*Knobs pins enabling*/
            APCTL1_ADPC2_MASK | APCTL1_ADPC1_MASK | APCTL1_ADPC0_MASK;
            
    APCTL2 = APCTL2_ADPC9_MASK;                                            /*Battery Level pin enabling*/
}


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   ADC Values Acquisition (Loop for reading all N channels,defined by ADC_CH define)
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  
void vfnReadADCValues(void)
{
    UINT8 u8ChCounter;
    for(u8ChCounter=0; u8ChCounter<ADC_CH; u8ChCounter++)
    {
        vfnReadADCH(u8ChCounter);  
    }
}

///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   ADC Channel Reading. Reads value of Channel "N", "N" been received as parameter
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  

void vfnReadADCH(UINT8 u8Channel)
{
    UINT8 Index = (u8Channel << 1); //use ADC channel to index buffer              /* Knob Jitter Filtering Start */
    UINT8 ADCVal;
    ADCSC1 = u8Channel;             
    while(!(ADCSC1 & 0x80));
    ADCVal = ADCRL;                 //set buffer for ADC read
     
      if((gu8adc_buf[Index] - gu8adc_buf[(Index + 1)]) >= 0)    //check if knob has been moving up
      {
        if((ADCVal - gu8adc_buf[Index]) < 0)                    //check if knob is now moving down
        {
           if((ADCVal - gu8adc_buf[Index]) > -4)                //require knob to move 4 positions in order to change value
           {
              ADCVal = gu8adc_buf[Index];
           }
        }
      }
      else //if((gu8adc_buf[Indx] - gu8adc_buf[(Indx + 1)]) < 0)   //check if knob has been moving down
      {
        if((ADCVal - gu8adc_buf[Index]) > 0)                       //check if knob is now moving up
        {
           if((ADCVal - gu8adc_buf[Index]) < 4)                    //require a knob to move 4 positions in order to change value
           {
              ADCVal = gu8adc_buf[Index];
           }
        }
      }
      
      if(!ADCRL) ADCVal = 0;                                       //if ADC read is 0 then set knob to be 0
    
    gua8Knobs[u8Channel] = ADCVal;    //8-bit value stored.
    gu8adc_buf[(Index + 1)] = gu8adc_buf[Index];                   //store buffer values for next read
    gu8adc_buf[Index] = ADCVal;                                                /* Knob Jitter Filtering End */
    
}
