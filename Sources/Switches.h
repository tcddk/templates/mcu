/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   Switches.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro \n B13061
\version
\date
\warning None
* History:
*/

#ifndef _SWITCHES_H
#define _SWITCHES_H

#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"
#include "GPIO.h"
#include "PublicVariables.h"



/* Function Prototypes */

void ReadSwitches(void);


#endif /* _SWITCHES_H */