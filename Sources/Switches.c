/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	Switches.c
  \brief    	
  \author   	Freescale Semiconductor
  \author     Diego Haro \n B13061
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    None

  * History:
  
*/
#include "Switches.h"




/*************SWITCHES READING***********

gua8Switches[0] = Tone module SW1
gua8Switches[1] = Tone module SW2
gua8Switches[2] = Tone dock SW1
gua8Switches[3] = Tone dock SW2

For the Tap ON/OFF Switch(DOCK_SW1)
Open  = 0
Close = 1

For the Foot Switch(DOCK_SW2)
Open  = 0
Close = 1
                                     
For the Module switches:
  The switch has 3 positions:
  Left = 0
  Center = 1
  Right = 2
  
***************************************/


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   Read Switches Function
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
    
    The reading of the switches is done like this:
    
    -For the Switches in the ToneCore Programmable Module:
      Every time this function is called one of the two switches is been read, SW_SEL is an output line where
      the MCU specifies which of the two switches is going to be read, because of Hardware constrains, it is
      only possible to read one at the time. The MCU translates the status in the SW_LP and SW_RP input lines, 
      to one of the positions of the switch (Left, Center, or Right) and stores the value in a buffer.
     
    -For the Switches in the ToneCore Programmer Dock:
      The MCU simply read the status of the two switches (DOCK_SW1 and DOCK_SW2), where if the line is in a 
      Logic 1 value, that means that the switch is open and a 0 is stored in the buffer and if a logical 0 
      is read it means that the switch is been pressed down and a 1 is stored in the buffer.
 
*/  
void ReadSwitches(void)
{  
  
     if(SW_SEL==1)
     {
       //ToneModule Switch 1
       if(!SW_LP)gua8Switches[0] = 0;
       else if(!SW_RP)gua8Switches[0] = 2;
       else gua8Switches[0] = 1;         
     }
     else
     {
       //ToneModule Switch 2
       if(!SW_LP)gua8Switches[1] = 0;
       else if(!SW_RP)gua8Switches[1] = 2;
       else gua8Switches[1] = 1;     
     }
     
      SW_SEL = !SW_SEL;            
        
      //ToneCore Tap Switch
     if(DOCK_SW1)gua8Switches[2] = 0;
     else gua8Switches[2] = 1;
   
     //ToneCore Foot Switch
     if(DOCK_SW2)gua8Switches[3] = 0;
     else gua8Switches[3] = 1;


}
