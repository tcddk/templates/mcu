#include "vectors.h"
/*
  The User's Reset vector is redirected to the _Startup which is the Entry point for this project, in this
  Entry point the Stach Pointer and the RAM are been initialized
*/
extern void _Startup(void); 



/*
  This is the declaration of the Dummy ISR's, just in case a Interrupt that is not defined here is requested, 
  The MCU will enter this infinit for loop.
*/
interrupt void vfnDummy_ISR(void) 
{
    for(;;);
}



/*
  This declaration indicates that the implementation of the RTC ISR is been done in a diferent file 
  (RTC.c in this case)
*/
extern void vfnRTC_ISR(void);


/*
  This macro defines the address of the USB Interrupt Service Routine located in Protected Flash section
  You can use your own USB ISR by declaring a function with the same name, and removing the macro.  
*/
#define vfnUSB_ISR  0xF800



/* 
The Interrupts vectors have been redirected from addresses 0xFFC0-0xFFFD to 0xF7C0-0xF7FD 
Due to the Flash Protection of the higher part of the memory 0xF800 to 0xFFFF to store the Bootloader
Part of the project that will always remain in the MCU to ensure the communication between the MCU and the GUI

You can create any ISR by replacing the correspondient vfnDummy_ISR name with the final ISR name, for example:
The vector for the RTC interruption is defined here (vfnRTC_ISR) as an extern interrupt since it is located in the 
RTC.c file, where the ISR function declared and implemented. 

*/
void (* volatile const _Usr_Vector[])()@0xF7C0= { 

    vfnDummy_ISR,        // Int.no.31 UNIMPLEMENTED      (at F7C0)
    vfnDummy_ISR,        // Int.no.30 UNIMPLEMENTED      (at F7C2)
    vfnRTC_ISR,          // Int.no.29 RTC                (at F7C4)
    vfnDummy_ISR,        // Int.no.28 IIC                (at F7C6)
    vfnDummy_ISR,        // Int.no.27 ACMP               (at F7C8)
    vfnDummy_ISR,        // Int.no.26 ADC                (at F7CA)
    vfnDummy_ISR,        // Int.no.25 KBI                (at F7CC)
    vfnDummy_ISR,        // Int.no.24 SCI2 Transmit      (at F7CE)
    vfnDummy_ISR,        // Int.no.23 SCI2 Receive       (at F7D0)
    vfnDummy_ISR,        // Int.no.22 SCI2 Error         (at F7D2)
    vfnDummy_ISR,        // Int.no.21 SCI1 Transmit      (at F7D4)
    vfnDummy_ISR,        // Int.no.20 SCI1 Receive       (at F7D6)
    vfnDummy_ISR,        // Int.no.19 SCI1 error         (at F7D8)
    vfnDummy_ISR,        // Int.no.18 TPM2 Overflow      (at F7DA)
    vfnDummy_ISR,        // Int.no.17 TPM2 CH1           (at F7DC)
    vfnDummy_ISR,        // Int.no.16 TPM2 CH0           (at F7DE)
    vfnDummy_ISR,        // Int.no.15 TPM1 Overflow      (at F7E0)
    vfnDummy_ISR,        // Int.no.14 TPM1 CH5           (at F7E2)
    vfnDummy_ISR,        // Int.no.13 TPM1 CH4           (at F7E4)
    vfnDummy_ISR,        // Int.no.12 TPM1 CH3           (at F7E6)
    vfnDummy_ISR,        // Int.no.11 TPM1 CH2           (at F7E8)
    vfnDummy_ISR,        // Int.no.10 TPM1 CH1           (at F7EA)
    vfnDummy_ISR,        // Int.no.9  TPM1 CH0           (at F7EC)
    vfnDummy_ISR,        // Int.no.8  Reserved           (at F7EE)
    vfnUSB_ISR,          // Int.no.7  USB Statue         (at F7F0)
    vfnDummy_ISR,        // Int.no.6  SPI2               (at F7F2)
    vfnDummy_ISR,        // Int.no.5  SPI1               (at F7F4)
    vfnDummy_ISR,        // Int.no.4  Loss of lock       (at F7F6)
    vfnDummy_ISR,        // Int.no.3  LVI                (at F7F8)
    vfnDummy_ISR,        // Int.no.2  IRQ                (at F7FA)
    vfnDummy_ISR,        // Int.no.1  SWI                (at F7FC)
    _Startup             // Redirected RESET Vector      (at F7FE)
};
