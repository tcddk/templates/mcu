 #include "BatteryLevel.h"
 
 
 
UINT8 u8LowBatt = 0;


   
 ///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   Battery Level reading function, 
            if battery's voltage is below the defined threshold in BatteryLevel.h a flag is set.
            
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  

 void ReadBatteryLevel(void) 
{   
    
    ADCSC1 = 9;
    while(!(ADCSC1 & 0x80));
    
    if(ADCRL<LOW_BATTERY_THRESHOLD_IN_ADC_COUNTS) 
    {
        u8LowBatt = 1;
    }
    else 
    {
        u8LowBatt = 0;
    }
                            
}