/**
  Copyright (c) 2006 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	SPI.c
  \brief    	SPI low level driver
  \author   	Freescale Semiconductor
  \author     	Jose Ruiz \n B03634
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    	1.0
  \date     	05/12/2006
  \warning    	None
*/
#include "SPI.h" 




///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   SPI Module Initialization
 * \author  Jose Ruiz \n B03634
 * \return  none
 * \warning none
*/  
void vfnSPISetup(void)
{    
    SPI1C1 = SPI1C1_MSTR_MASK|SPI1C1_CPHA_MASK;// CPOL =0, CPHA=1
    SPI1C2 = 0x00;     
    SPI1BR = 0x70;     	// Baud Rate = BusCLK/16 ~= 1.5 MHz 
   	SPI1C1_SPE=1;		// Enable SPI
}


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   SPI Send one byte
 * \author  Jose Ruiz \n B03634
 * \return  none
 * \warning none
*/  
void vfnSPI_SendByte(UINT8 u8Data)
{
  	(void)SPI1S;	
  	SPI1DL=u8Data;
    while(!SPI1S_SPTEF);	  
}


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   SPI Recieve one byte
 * \author  Jose Ruiz \n B03634
 * \return  none
 * \warning none
*/  
UINT8 vfnSPI_ReceiveByte(void)
{	
    (void)SPI1S;
    SPI1DL=0x00;           //write a 0 (dummy write)
    while(!SPI1S_SPRF);
    return(SPI1DL);  
}


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   SPI 24bits Data sent to DSP 
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  
void vfnSPI_SendWord(UINT32 u32Temporal)
{
    vfnSPI_SendByte((UINT8)(u32Temporal>>16));	// Higher byte of data sent to DSP 
    vfnSPI_SendByte((UINT8)(u32Temporal>>8));		// Middle byte of data sent to DSP  
    vfnSPI_SendByte((UINT8)(u32Temporal));			// Lower  byte of data sent to DSP  
}

///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   SPI 24bits Data received from DSP 
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  
UINT32 vfnSPI_ReceiveWord(void)
{
    UINT32 u32ReceivedWord;
    
    u32ReceivedWord  = ((UINT32)vfnSPI_ReceiveByte()) <<16;
    u32ReceivedWord += ((UINT32)vfnSPI_ReceiveByte()) <<8;    
    u32ReceivedWord += (UINT8)vfnSPI_ReceiveByte();
      
    return(u32ReceivedWord);
}

