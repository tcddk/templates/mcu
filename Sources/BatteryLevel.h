/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   BatteryLevel.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro \n B13061
\version
\date
\warning None
* History:
*/

#ifndef _BatteryLevel_H
#define _BatteryLevel_H


#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"
#include "GPIO.h"
#include "RTC.h"
#include "PublicVariables.h"


/* Defines */
#define BATTERY_LEVEL_LED_RATE                               3200 /*Multiply this number by 31.25us to get the actual toggle rate when in low batt*/

#define LOW_BATTERY_THRESHOLD_IN_VOLTS                         7 /*<---- You can modify this integer value (Between 6 and 8)*/
                                                                 /*This value is in volts, if the voltage in the battery goes below this value,
                                                                   The MCU will detect it as Low Battery and will let you know by toggling the RED LED
                                                                 */

#define LOW_BATTERY_THRESHOLD_IN_ADC_COUNTS    ((LOW_BATTERY_THRESHOLD_IN_VOLTS*18)+12)/*Do not modify this value, it is the low battery limit in ADC Counts*/

/* Declarations */
extern UINT8 u8LowBatt;

/* Function Prototypes */
void ReadBatteryLevel(void); 


#endif /* _BatteryLevel_H */