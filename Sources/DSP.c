/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	DSP.c
  \brief    	Driver to Bootload and periodically send/receive Data to/from
              DSP.
  \author   	Freescale Semiconductor
  \author     Diego Haro \n B13061
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    None

  * History:
  
*/

#include "DSP.h"
#include "PublicVariables.h"
                                                      
UINT8 gu8DSP_TransceiverActualState = DSP_STATE_IDLE; /* This variable is used to know the actual state 
                                                         of the transciever */

UINT8 gu8DSP_CommunicationActualState = 0;                     /* Used to know the Main actual state of the DSP state machine */

UINT32 gu32Address;                                   /* This variable is used by the Transciever state machine 
                                                         to know the address where the data will be stored */

UINT32 gu32Data;                                      /* This is the data that will be sent to the DSP when requested */

UINT8  gu8Knob_buf[6];

/* This is the compiled code for the DSP that expands the DSP program memory to 1.25k
   and initializes DSP X and Y memory spaces from SPI */
const UINT8 gu8CodeToInitDSP_RAM[] =
{
  0x0A, 0xFA, 0x67, 0x31, 0xA9, 0x00, 0x04, 0xD1, 0x91, 0x30, 0x00, 0x00,
  0x06, 0x00, 0x84, 0x00, 0x00, 0x09, 0x01, 0x91, 0x91, 0x00, 0x00, 0x06,
  0x07, 0x58, 0x14, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x06, 0x00, 0x83,
  0x00, 0x00, 0x10, 0x01, 0x91, 0x91, 0x00, 0x00, 0x0D, 0x07, 0x58, 0x54,
  0x00, 0x00, 0x00, 0x0A, 0xF0, 0x80, 0xFF, 0x00, 0x00
};

#define DSP_MEMORY_INIT_CODE           (UINT8*) &gu8CodeToInitDSP_RAM           /* Location where the code is stored */
#define DSP_MEMORY_INIT_SIZE           (UINT32) 19                              /* Size in words of the compiled code */
#define DSP_MEMORY_INIT_START_ADDR     0                                       /* RAM address where the code will be located */

#define DSP_X_MEMORY_SIZE   1024    /* 1k * 1024 */
#define DSP_Y_MEMORY_SIZE   768     /* 0.75k * 1024 */

/**
 * \brief   This function will bootload the DSP with the code stored in location 'FLASH_START_ADDRESS_DSP_CODE'
            This function is divided in 3 steps:
              0. Reset DSP 
              1. Send code to expand the program RAM memory in DSP and initialize X and Y RAM (This step is optional)
              2. Send the number of words that will be transfered
              3. Send the starting address of RAM where the code in the DSP will be stored
              4. Send next word (DSP code) until finish
            
            The above steps are executed one by one every time the function is called, so it doesn't
            block the MCU resources.
            
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  

void vfnDSP_Bootload(void)
{
    /* The first step is to configure the DSP to expand the program RAM size to 1.25k
       and then configure the SPI to get the X and Y RAM data values from MCU.
       This is performed by downloading the code stored in DSP_MEMORY_INIT_CODE.
       
       After that the bootload starts as described below:
    
       At this moment the DSP code is located in the FLASH_START_ADDRESS_DSP_CODE in Flash.
       The first word is the code size (in words) and the second word is the starting RAM addres for
       the DSP to bootlad.

       As reflected in section 4.4 of the 56364 User Manual, the factory programmed
       bootstrap program expects the following:
          1. Three bytes defining the number of 24-bit program words to be loaded
          2. Three bytes defining the 24-bit start address to which the program loads
              in the 56364 program memory
          3. The program, three bytes for each 24 bit program word.
     */

                                        /* These variables are defined static so they don't loose their values
                                           after this function exits */
    static UINT8  *ptrDSPCode;          /* This will be the pointer to the bytes that will be sent to the DSP */
    static UINT32 u32CodeSize;          /* This variable will store the number of words to be sent to the DSP */
    static UINT8 u8BootLoadStep = 0;    /* This variable is used to know the actual state of this function    */
    static UINT8 u8BootLoadSubStep = 0; /* This variable is used to know the sub step to expand DSP memory  */
    
    if (u8BootLoadStep == 0)
    {
        /* The step 0 is to reset the DSP */               
        UINT16 u16x = 0;
        
        MASTER_RST = 0;             /* set reset line down for min 500 ns min */
        while(u16x++<6000);
        MASTER_RST = 1;
        
      #ifdef INIT_DSP_MEMORY      /* Delete INIT_DSP_MEMORY from DSP.h to avoid expanding the program memory of DSP
                                     and initializing X and Y data RAM */
        u8BootLoadStep++;           
      #else
        u8BootLoadStep = 2;         /* If EXPAND_DSP_MEMORY not defined, skip the step of expanding the program memory of DSP */
      #endif
    }

    else if (u8BootLoadStep == 1)
    {
        /* Step 1 is sending the instructions to expand the DSP RAM to 1.25k
           and initialize X and Y data RAM. This step is optional.
        
           This step is sub-divided in 5 steps (u8BootLoadSubStep):
              0. Send the size of the code (DSP_MEMORY_INIT_SIZE = 19)
              1. Send the address where the code will be stored (DSP_MEMORY_INIT_START_ADDR = 0)
              2. Send the words of code
              3. Send the values for X RAM stored in DSP_XData
              4. Send the values for Y RAM stored in DSP_YData
        */                 
        
        if (u8BootLoadSubStep == 0)
        {

            if (DSP_HREQ_READY)
            {                                
                SET_SS();                                         /* Set SS line to start SPI transfer  */
                
                ptrDSPCode = (UINT8*) DSP_MEMORY_INIT_CODE;  /* Point to where the code is stored in Flash */
                
                u32CodeSize = DSP_MEMORY_INIT_SIZE;
                vfnSPI_SendWord(u32CodeSize);                         /* Send the code size to the DSP      */
                
                u8BootLoadSubStep++;
            }
          
        }

        else if (u8BootLoadSubStep == 1)
        {
            if (DSP_HREQ_READY)
            {
                vfnSPI_SendWord(DSP_MEMORY_INIT_START_ADDR);     /* Send the starting address where code will be stored */              
                u8BootLoadSubStep++;
            }
        }

        else if (u8BootLoadSubStep == 2)
        {
            if (u32CodeSize)   /* if there are words to send  */
            {
                if (DSP_HREQ_READY)
                {            
                    vfnSPI_SendByte(*ptrDSPCode++);    /* Send the word  */
                    vfnSPI_SendByte(*ptrDSPCode++); 
                    vfnSPI_SendByte(*ptrDSPCode++); 
                    u32CodeSize--;
                }                                    /* Decrement counter  */
            }
            else
            {
                /* If there are no words to send  */
                
                CLEAR_SS();                           /* Clear SS line to finish SPI transfer   */
                u8BootLoadSubStep++;
                ptrDSPCode = (UINT8*) FLASH_START_ADDRESS_DSP_XDATA;
                u32CodeSize = DSP_X_MEMORY_SIZE;
                SET_SS();
            }
        }
        
        else if (u8BootLoadSubStep == 3)
        {
            if (u32CodeSize)
            {
                if (DSP_HREQ_READY)
                {            
                    vfnSPI_SendByte(*ptrDSPCode++);
                    vfnSPI_SendByte(*ptrDSPCode++); 
                    vfnSPI_SendByte(*ptrDSPCode++); 
                    u32CodeSize--;
                }
            }
            else
            {
                u8BootLoadSubStep++;
                ptrDSPCode = (UINT8*) FLASH_START_ADDRESS_DSP_YDATA;
                u32CodeSize = DSP_Y_MEMORY_SIZE;
            }
        }
        
        else if (u8BootLoadSubStep == 4)
        {
            if (u32CodeSize)
            {
                if (DSP_HREQ_READY)
                {            
                    vfnSPI_SendByte(*ptrDSPCode++);
                    vfnSPI_SendByte(*ptrDSPCode++); 
                    vfnSPI_SendByte(*ptrDSPCode++); 
                    u32CodeSize--;
                }
            }
            else
            {
                u8BootLoadSubStep = 0;
                u8BootLoadStep++;
                CLEAR_SS();
            }
        }
    }

    else if (u8BootLoadStep == 2)
    {
        /* The step 2 is to send the Code Size to DSP */       
                                       
        ptrDSPCode = (UINT8*) FLASH_START_ADDRESS_DSP_CODE_SIZE;
 
        u32CodeSize =  ((UINT32)(*ptrDSPCode++)) << 16;
        u32CodeSize += ((UINT32)(*ptrDSPCode++)) << 8;
        u32CodeSize += ((UINT32)(*ptrDSPCode++));
        
        if (DSP_HREQ_READY)
        {                                
            SET_SS();                   /* Set SS line to start SPI transfer  */
            vfnSPI_SendWord(u32CodeSize);   /* Send the code size to the DSP      */
            u8BootLoadStep++;           /* change to next step so when this function is executed again it will go to step 1 */
        }
         
    }    
    else if (u8BootLoadStep == 3)
    {
        /* The next step is to send the starting address of DSP RAM where the DSP code will be stored  */      
        
        UINT32 u32DSPCodeStartAddr;
        
        ptrDSPCode = (UINT8*) FLASH_START_ADDRESS_DSP_CODE_START_ADDR;
        
        u32DSPCodeStartAddr =  ((UINT32)(*ptrDSPCode++)) << 16;
        u32DSPCodeStartAddr += ((UINT32)(*ptrDSPCode++)) << 8;
        u32DSPCodeStartAddr += ((UINT32)(*ptrDSPCode++));
        
        if (DSP_HREQ_READY)
        {
            vfnSPI_SendWord(u32DSPCodeStartAddr);   /* Transfer starting address */
            ptrDSPCode = (UINT8*) FLASH_START_ADDRESS_DSP_CODE;
            u8BootLoadStep++;                   /* Go to next step */
        }
    }
    else if (u8BootLoadStep == 4)/* Start sending all code to DSP */
    {        
        /* This step will send all the DSP code. It will send one word by each execution, 
           so it doesn't block the main application */
        
        if (u32CodeSize)   /* if there are words to send  */
        {
            if (DSP_HREQ_READY)
            {            
                vfnSPI_SendByte(*ptrDSPCode++);    /* Send the word  */
                vfnSPI_SendByte(*ptrDSPCode++); 
                vfnSPI_SendByte(*ptrDSPCode++); 
                u32CodeSize--;
            }                                    /* Decrement counter  */
        }
        else
        {
            /* If there are no words to send  */
            
            u8BootLoadStep = 0;                   /* Restore Step to 0  */
            CLEAR_SS();                           /* Clear SS line to finish SPI transfer   */
            gu8DSP_Flags.SendDataToDSP = 1;       /* Set the flag to start sending data to DSP   */
            gu16RTC_UpdateDSPTimer = DSP_STARTUP_TIME;  /* Set the RTC timer to 255 ms.
                                                     The MCU will not send data to the DSP until 
                                                     this time has elapsed. This is to give enough time to the 
                                                     DSP to initialize   */
        }
    }
        
}









/**
 * \brief   This is a pointer to functions. It handles the communication with the DSP through SPI.            
            As described in the TCDDK documentation, the SPI communication is defined as:
            
            1. Three bytes defining the command type (Read or Write)                
            2. Three bytes defining the address where the data will be stored, or the address that we want to read
            3. Three bytes defining the data in the case of a write command, or a dummy read in the case of a read command
            4. In the case of a read command, the data will be returned where this 3 bytes are read

            Note: All of the Command types and variables address in the DSP are defined in PublicVariables.h

            This pointer to function will jump to one of the following functions depending
            on the value of gu8DSP_TransceiverActualState:

                vfnDSP_StateIdle,            
                vfnDSP_StateTx,            
                vfnDSP_StateRx                        
            
 * \author  Samuel Quiroz \n B05113
*/

void (*const vfnDSP_TransceiverStateMachine[])(void) = 
{
    vfnDSP_StateIdle,
    vfnDSP_StateTx,
    vfnDSP_StateRx
};


 
/** 
 * \brief     Write data to the DSP. This function only stores the data and address to be sent
              and sets the proper state to the TranscieverActualState  
 * \author    Samuel Quiroz. B05113
 * \param     u32Address. The address in the DSP RAM where the data will be stored
 * \param     u32Data. The data that will be sent
 * \return    none
 * \warning 
 */   
static void vfnDSP_WriteData(UINT32 u32Address, UINT32 u32Data)
{
    gu32Address = u32Address;
    gu32Data = u32Data;    
    gu8DSP_TransceiverActualState = DSP_STATE_TX;
}


/** 
 * \brief     Read data from the DSP. This function only stores the address to be read
              and sets the proper state to the TranscieverActualState.
              When the state Transciever state machine finishes the read transaction,
              the retrieved data will be stored in the global variable: gu32Data
 * \param     u32Address. The address in the DSP RAM where the data that we want to read is stored
 * \return    none
 * \warning 
 */   
static void vfnDSP_ReadData(UINT32 u32Address)
{
    gu32Address = u32Address;
    gu8DSP_TransceiverActualState = DSP_STATE_RX;
}



/********************************************************************/
/*          States of the transciever for the DSP                   */
/********************************************************************/

/** 
 * \brief     This state is idle, it does nothing
 * \return    none
 * \warning 
 */ void vfnDSP_StateIdle(void)
{  
}


/**
 * \brief   Send Data to DSP State Machine. When it finishes a write operation, 
            the gu8DSP_Flags.DataSent is set to 1 and the state is changed to DSP_STATE_IDLE
            
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_StateTx(void)
{
    static UINT8 u8TxState = TX_STATE_CMD;    /* This variable is used to store the actual state of the 
                                                 transmission status. */
        
    switch (u8TxState)
    {
        case TX_STATE_CMD:                    /* The 1st step is to send the write command */
        {
            if (DSP_HREQ_READY)
            {                                                
                vfnSPI_SendWord(CMD_WRITE_TO_DSP);
                u8TxState = TX_STATE_ADDR;
            }            
            break;
        }

        case TX_STATE_ADDR:                   /* The 2nd state is to send the address  */
        {
            if (DSP_HREQ_READY)
            {
                vfnSPI_SendWord(gu32Address);
                u8TxState = TX_STATE_DATA;
            }
            break;
        }

        case TX_STATE_DATA:                   /* The 3rd step is to send the data   */
        {
            if (DSP_HREQ_READY)
            {
                vfnSPI_SendWord(gu32Data);
                u8TxState = TX_STATE_CMD;     /* change my state to send the command (next time a read is performed) */
                gu8DSP_TransceiverActualState = DSP_STATE_IDLE;    /* return to idle state         */
                gu8DSP_Flags.DataSent = 1;    /* Set a flag to indicate that data has been sent    */
            }
            break;
        }
    }  
}


/**
 * \brief   Read Data from DSP State Machine. When it finishes a read operation, 
            the gu8DSP_Flags.DataSent is set to 1 and the state is changed to DSP_STATE_IDLE
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_StateRx(void)
{
    static UINT8 u8RxState = RX_STATE_CMD;      /* The 1st step is to send the read command  */
    
    switch (u8RxState)
    {
        case RX_STATE_CMD:
        {
            if (DSP_HREQ_READY)
            {
                vfnSPI_SendWord(CMD_READ_FROM_DSP);
                u8RxState = RX_STATE_ADDR;
            }            
            break;
        }

        case RX_STATE_ADDR:                     /* The 2nd state is to send the address  */
        {
            if (DSP_HREQ_READY)
            {
                vfnSPI_SendWord(gu32Address);
                u8RxState = RX_STATE_DATA0;
            }
            break;
        }

        case RX_STATE_DATA0:                    /* The 3rd step is to perform a dummy read  */
        {
            if (DSP_HREQ_READY)
            {
                (void)vfnSPI_ReceiveWord();         /* dummy read  */              
                u8RxState = RX_STATE_DATA1;
            }
            break;
        }
        
        case RX_STATE_DATA1:                     /* The 4th step is to perform the read operation */
        {
            if (DSP_HREQ_READY)
            {
                gu32Data = vfnSPI_ReceiveWord();
                u8RxState = TX_STATE_CMD;        /* change my state to send the command (next time a read is performed) */
                gu8DSP_TransceiverActualState = DSP_STATE_IDLE;     /* return to idle state                             */
                gu8DSP_Flags.DataSent = 1;       /* set flag to indicate that the read was performed                    */
            }
            break;
        }
    }   
}



/**
 * \brief
            This is a pointer to functions, it can have any of the values shown below, depending
            on the value of gu8DSP_CommunicationActualState.
            It will call each of this functions in the order they appear in order to read
            all knobs, switches, foot switches, LEDs and debug variables from the DSP
            
 * \author  Samuel Quiroz \n B05113
 */
void (*const vfnDSP_CommunicationStateMachine[])(void) = 
{
    vfnDSP_Knob1,
    vfnDSP_Knob2,
    vfnDSP_Knob3,
    vfnDSP_Knob4,
    vfnDSP_Knob5,
    vfnDSP_Knob6,
    vfnDSP_Switch1,
    vfnDSP_Switch2,
    vfnDSP_FootSwitchTap,
    vfnDSP_FootSwitch,
    vfnDSP_LEDRed,
    vfnDSP_LEDRedPWM,
    vfnDSP_LEDGreen,
    vfnDSP_LEDGreenPWM,
    vfnDSP_DebugVar1ToDSP,
    vfnDSP_DebugVar2ToDSP,
    vfnDSP_DebugVar3ToDSP,
    vfnDSP_DebugVar4ToDSP,
    vfnDSP_DebugVar1FromDSP,
    vfnDSP_DebugVar2FromDSP,
    vfnDSP_DebugVar3FromDSP,
    vfnDSP_DebugVar4FromDSP    
};



 
/** 
 * \brief     This functions calls the state machine for the DSP Main state machine and
              for the DSP transciever state machine. It should be called from the main application.
 * \author    Samuel Quiroz. B05113
 * \return    none
 */   
void vfnStateMachineDSP(void)
{
    vfnDSP_CommunicationStateMachine[gu8DSP_CommunicationActualState]();            
    vfnDSP_TransceiverStateMachine[gu8DSP_TransceiverActualState]();    
}



/***************************************************************************************************************************
 *
 *    Each of the functions below will perform a read or a write to the DSP.
 *    It will do the following:
 *        1. When the Transciever is Idle it will start a read or write transaction
 *        2. When the Transciever has finished the transaction:
 *            - If it was a read operation, it will store the received data in the correct variable (See PublicVariables.c)
 *            - Move to the next state. If the last state has been reached, it will return to the first one. 
 *
 *    NOTE 1: Knobs values are read and stored as bytes but they are sent to the DSP shifted 15 positions 
 *            to the left in order to have a range from 0x000000 to 0x7F80000, instead of 0x00 to 0xFF
 *            This is because the DSP makes calculations using 24bit numbers
 *
 *    NOTE 2: Debug variables are stored as t_UINT24 (like an array of 3 bytes)
 *            This is why they are being read or written as shifted bytes
 * 
 ****************************************************************************************************************************/

/**
 * \brief   Value of Knob1 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
**/  
static void vfnDSP_Knob1(void)
{ 
  UINT32 Knob_avg;                                      /* Knob Jitter Filtering Start */
  UINT32 Knob_val;
    
    Knob_avg = (gu8Knob_buf[0] + KNOB_1) >> 1;          //Average current and previous knob values
    Knob_val = Knob_avg << 15;                          //extend knob value to a 24 bit value
    Knob_val += Knob_avg << 7;                          //add multiples of knob value so that KNOB_MAX = 0x7FFFFF
    Knob_val += Knob_avg >> 1;
    gu8Knob_buf[0] = KNOB_1;                            /* Knob Jitter Filtering End */
    
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(KNOB_1_ADDR_DSP,(Knob_val));
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }
}


/**
 * \brief   Value of Knob2 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Knob2(void)
{
  UINT32 Knob_avg;                                      /* Knob Jitter Filtering Start */
  UINT32 Knob_val;
    
    Knob_avg = (gu8Knob_buf[1] + KNOB_2) >> 1;          //Average current and previous knob values
    Knob_val = Knob_avg << 15;                          //extend knob value to a 24 bit value
    Knob_val += Knob_avg << 7;                          //add multiples of knob value so that KNOB_MAX = 0x7FFFFF
    Knob_val += Knob_avg >> 1;
    gu8Knob_buf[1] = KNOB_2;                            /* Knob Jitter Filtering End */
    
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(KNOB_2_ADDR_DSP,Knob_val);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)   */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Knob3 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Knob3(void)
{
  UINT32 Knob_avg;                                      /* Knob Jitter Filtering Start */
  UINT32 Knob_val;
    
    Knob_avg = (gu8Knob_buf[2] + KNOB_3) >> 1;          //Average current and previous knob values
    Knob_val = Knob_avg << 15;                          //extend knob value to a 24 bit value
    Knob_val += Knob_avg << 7;                          //add multiples of knob value so that KNOB_MAX = 0x7FFFFF
    Knob_val += Knob_avg >> 1;
    gu8Knob_buf[2] = KNOB_3;                            /* Knob Jitter Filtering End */

    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(KNOB_3_ADDR_DSP,Knob_val);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)   */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Knob4 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Knob4(void)
{
  UINT32 Knob_avg;                                      /* Knob Jitter Filtering Start */
  UINT32 Knob_val;
    
    Knob_avg = (gu8Knob_buf[3] + KNOB_4) >> 1;          //Average current and previous knob values
    Knob_val = Knob_avg << 15;                          //extend knob value to a 24 bit value
    Knob_val += Knob_avg << 7;                          //add multiples of knob value so that KNOB_MAX = 0x7FFFFF
    Knob_val += Knob_avg >> 1;
    gu8Knob_buf[3] = KNOB_4;                            /* Knob Jitter Filtering End */

    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(KNOB_4_ADDR_DSP,Knob_val);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1) */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Knob5 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Knob5(void)
{
  UINT32 Knob_avg;                                      /* Knob Jitter Filtering Start */
  UINT32 Knob_val;
    
    Knob_avg = (gu8Knob_buf[4] + KNOB_5) >> 1;          //Average current and previous knob values
    Knob_val = Knob_avg << 15;                          //extend knob value to a 24 bit value
    Knob_val += Knob_avg << 7;                          //add multiples of knob value so that KNOB_MAX = 0x7FFFFF
    Knob_val += Knob_avg >> 1;
    gu8Knob_buf[4] = KNOB_5;                            /* Knob Jitter Filtering End */

    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(KNOB_5_ADDR_DSP,Knob_val);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Knob6 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Knob6(void)	
{
  UINT32 Knob_avg;                                      /* Knob Jitter Filtering Start */
  UINT32 Knob_val;
    
    Knob_avg = (gu8Knob_buf[5] + KNOB_6) >> 1;          //Average current and previous knob values
    Knob_val = Knob_avg << 15;                          //extend knob value to a 24 bit value
    Knob_val += Knob_avg << 7;                          //add multiples of knob value so that KNOB_MAX = 0x7FFFFF
    Knob_val += Knob_avg >> 1;
    gu8Knob_buf[5] = KNOB_6;                            /* Knob Jitter Filtering End */

    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(KNOB_6_ADDR_DSP,Knob_val);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Switch1 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Switch1(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(SWITCH_1_ADDR_DSP,SWITCH_1);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)   */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Switch2 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_Switch2(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(SWITCH_2_ADDR_DSP,SWITCH_2);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)    */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Foot Switch Tap sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_FootSwitchTap(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(FOOT_SWITCH_TAP_ADDR_DSP,FOOT_SWITCH_TAP);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)   */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Foot Switch sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_FootSwitch(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_WriteData(FOOT_SWITCH_ADDR_DSP,FOOT_SWITCH);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)   */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Red LED read from DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_LEDRed(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_ReadData(RED_LED_ADDR_DSP);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)    */
        {                
            RED_LED = (UINT8)gu32Data;
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }   
}

static void vfnDSP_LEDRedPWM(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_ReadData(RED_LED_PWM_ADDR_DSP);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1)    */
        {                
            RED_LED_PWM_VAL = (UINT16)gu32Data;
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }   
}


/**
 * \brief   Value of Green LED read from DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_LEDGreen(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_ReadData(GREEN_LED_ADDR_DSP);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1) */
        {                
            GREEN_LED = (UINT8)gu32Data;
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}

static void vfnDSP_LEDGreenPWM(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            vfnDSP_ReadData(GREEN_LED_PWM_ADDR_DSP);  
        }
        else /* if(gu8DSP_Flags.DataSent == 1) */
        {                
            GREEN_LED_PWM_VAL = (UINT16)gu32Data;
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Debug Variable1 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar1ToDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            gu32Data =  ((UINT32)DEBUG_VAR_TO_DSP_1.Byte2)<<16;            
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_1.Byte1)<<8;
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_1.Byte0);

            SET_SS();                                                
            vfnDSP_WriteData(DEBUG_VAR_TO_DSP_1_ADDR_DSP, gu32Data);
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Debug Variable2 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar2ToDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            gu32Data =  ((UINT32)DEBUG_VAR_TO_DSP_2.Byte2)<<16;            
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_2.Byte1)<<8;
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_2.Byte0);
            
            vfnDSP_WriteData(DEBUG_VAR_TO_DSP_2_ADDR_DSP, gu32Data);
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Debug Variable3 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar3ToDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            gu32Data =  ((UINT32)DEBUG_VAR_TO_DSP_3.Byte2)<<16;            
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_3.Byte1)<<8;
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_3.Byte0);
            
            vfnDSP_WriteData(DEBUG_VAR_TO_DSP_3_ADDR_DSP, gu32Data);
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    } 
}


/**
 * \brief   Value of Debug Variable4 sent to DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar4ToDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                    
            gu32Data =  ((UINT32)DEBUG_VAR_TO_DSP_4.Byte2)<<16;            
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_4.Byte1)<<8;
            gu32Data += ((UINT32)DEBUG_VAR_TO_DSP_4.Byte0);
            
            vfnDSP_WriteData(DEBUG_VAR_TO_DSP_4_ADDR_DSP, gu32Data);
        }
        else /* if(gu8DSP_Flags.DataSent == 1) */
        {                
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Debug Variable1 read from DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar1FromDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                                
            vfnDSP_ReadData(DEBUG_VAR_FROM_DSP_1_ADDR_DSP);
        }
        else /* if(gu8DSP_Flags.DataSent == 1) */
        {                
            DEBUG_VAR_FROM_DSP_1.Byte2 = (UINT8) (gu32Data>>16);
            DEBUG_VAR_FROM_DSP_1.Byte1 = (UINT8) (gu32Data>>8);
            DEBUG_VAR_FROM_DSP_1.Byte0 = (UINT8) (gu32Data);
                        
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}


/**
 * \brief   Value of Debug Variable2 read from DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar2FromDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                                
            vfnDSP_ReadData(DEBUG_VAR_FROM_DSP_2_ADDR_DSP);
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            DEBUG_VAR_FROM_DSP_2.Byte2 = (UINT8) (gu32Data>>16);
            DEBUG_VAR_FROM_DSP_2.Byte1 = (UINT8) (gu32Data>>8);
            DEBUG_VAR_FROM_DSP_2.Byte0 = (UINT8) (gu32Data);
                        
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}



/**
 * \brief   Value of Debug Variable3 read from DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar3FromDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                                
            vfnDSP_ReadData(DEBUG_VAR_FROM_DSP_3_ADDR_DSP);
        }                                         
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            DEBUG_VAR_FROM_DSP_3.Byte2 = (UINT8) (gu32Data>>16);
            DEBUG_VAR_FROM_DSP_3.Byte1 = (UINT8) (gu32Data>>8);
            DEBUG_VAR_FROM_DSP_3.Byte0 = (UINT8) (gu32Data);
                        
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState++;
        }                                
    }  
}




/**
 * \brief   Value of Debug Variable4 read from DSP
 * \author  Samuel Quiroz \n B05113
 * \return  none
 * \warning none
*/  
static void vfnDSP_DebugVar4FromDSP(void)
{
    if (gu8DSP_TransceiverActualState == DSP_STATE_IDLE)
    {
        if (gu8DSP_Flags.DataSent == 0)
        {
            SET_SS();                                                
            vfnDSP_ReadData(DEBUG_VAR_FROM_DSP_4_ADDR_DSP);
        }
        else /* if(gu8DSP_Flags.DataSent == 1)  */
        {                
            DEBUG_VAR_FROM_DSP_4.Byte2 = (UINT8) (gu32Data>>16);
            DEBUG_VAR_FROM_DSP_4.Byte1 = (UINT8) (gu32Data>>8);
            DEBUG_VAR_FROM_DSP_4.Byte0 = (UINT8) (gu32Data);
                        
            gu8DSP_Flags.DataSent = 0;
            CLEAR_SS();
            gu8DSP_CommunicationActualState=0;
        }                                
    }  
}
