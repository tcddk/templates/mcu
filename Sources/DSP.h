/**
  Copyright (c) 2008 Freescale Semiconductor
  \file   DSP.h
  \brief
  \author Freescale Semiconductor
  \author Guadalajara Applications Laboratory RTAC Americas
  \author Samuel Quiroz \n B05113
  \version
  \date
  \warning Nonce
* History:
*/


#ifndef _DSP_H
#define _DSP_H


#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"    /* include macros & defines declarations */
#include "GPIO.h"
#include "ADC.h"
#include "SPI.h"
#include "RTC.h"
#include "Switches.h"



/* Defines */

#define FLASH_START_ADDRESS_DSP_CODE_SIZE           0xC000
#define FLASH_START_ADDRESS_DSP_CODE_START_ADDR     0xC003
#define FLASH_START_ADDRESS_DSP_CODE                0xC006            //Starting address where the DSP code will be stored in the MCU
#define FLASH_START_ADDRESS_DSP_XDATA               0xD000
#define FLASH_START_ADDRESS_DSP_YDATA               0xDC00

#define DSP_HREQ_READY                  SPI_HREQ == 0     //Used to know if the DSP is ready for a SPI transfer
#define SET_SS()                        SS_DOCK = 0       //Used to start a SPI transfer
#define CLEAR_SS()                      SS_DOCK = 1       //Used to stop a SPI transfer


#define DSP_STARTUP_TIME                8000         /* Set the RTC timer to 250 ms (8000 * 31.25us = 250ms).
                                                     The MCU will not send data to the DSP until 
                                                     this time has elapsed. This is to give enough time to the 
                                                     DSP to initialize 
                                                     The max value is 65535 which gives a startup time of aprx. 2 seconds*/

#define INIT_DSP_MEMORY              /* Delete this line to avoid expanding the program memory of DSP */

/* Declarations */

typedef enum
{
    DSP_STATE_IDLE,
    DSP_STATE_TX,
    DSP_STATE_RX,
}_States;


typedef enum
{
    TX_STATE_CMD,
    TX_STATE_ADDR,
    TX_STATE_DATA
} _TxStates;

typedef enum
{
    RX_STATE_CMD,
    RX_STATE_ADDR,
    RX_STATE_DATA0,
    RX_STATE_DATA1
} _RxStates;



/* Function Prototypes */

/* Public functions: */
void vfnStateMachineDSP(void);
void vfnDSP_Bootload(void);

/* Private functions: */
static void vfnDSP_WriteData(UINT32 u32Address, UINT32 u32Data);
static void vfnDSP_ReadData(UINT32 u32Address);
static void vfnDSP_StateIdle(void);
static void vfnDSP_StateTx(void);
static void vfnDSP_StateRx(void);


static void vfnDSP_Knob1(void);
static void vfnDSP_Knob2(void);
static void vfnDSP_Knob3(void);
static void vfnDSP_Knob4(void);
static void vfnDSP_Knob5(void);
static void vfnDSP_Knob6(void);
static void vfnDSP_Switch1(void);
static void vfnDSP_Switch2(void);
static void vfnDSP_FootSwitchTap(void);
static void vfnDSP_FootSwitch(void);
static void vfnDSP_LEDRed(void);
static void vfnDSP_LEDRedPWM(void);
static void vfnDSP_LEDGreen(void);
static void vfnDSP_LEDGreenPWM(void);
static void vfnDSP_DebugVar1ToDSP(void);
static void vfnDSP_DebugVar2ToDSP(void);
static void vfnDSP_DebugVar3ToDSP(void);
static void vfnDSP_DebugVar4ToDSP(void);
static void vfnDSP_DebugVar1FromDSP(void);
static void vfnDSP_DebugVar2FromDSP(void);
static void vfnDSP_DebugVar3FromDSP(void);
static void vfnDSP_DebugVar4FromDSP(void);



#endif /* _DSP_H */