/**
  Copyright (c) 2006 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	SPI.h
  \brief    	SPI low level driver
  \author   	Freescale Semiconductor
  \author     	Jose Ruiz \n B03634
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    	1.0
  \date     	05/12/2006
  \warning    	None
*/

#ifndef _SPI_H
#define _SPI_H

#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"



/* Function Prototypes */

void vfnSPISetup(void);
void vfnSPI_SendByte(UINT8 u8Data);
UINT8 vfnSPI_ReceiveByte(void);

void vfnSPI_SendWord(UINT32 u32Temporal);
UINT32 vfnSPI_ReceiveWord(void);




#endif  /* _SPI_H */