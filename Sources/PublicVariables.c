/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	PublicVariables.c
  \brief    	This file contains the variable declaration for the variables that are
              shared between the user's code and the USB handler.
  \author   	Freescale Semiconductor
  \author     Samuel Quiroz \n B05113
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    Any variable declared explicitly in the same memory address than the
              ones declared here will have conflict.
              User should not modify this file, only use the variables declared here
              to send the variables values the GUI.

  * History:
  
*/

#include "PublicVariables.h"




/*
   This is an array that reserves the memory location for knobs, switches, foot, switchs, LEDs and debug vars.
   It can be accessed directly or by using the macros defined in PublicVariables.h
*/
UINT8 gua8PublicVariables[36]     @ RAM_START_ADDR =
{
  0,0,0,0,0,0,  //6 knobs
  0,0,0,0,      //2 switches and 2 footswitches
  0,0,          //2 leds  
  0,0,0,        //4 debug variables from DSP
  0,0,0,
  0,0,0,
  0,0,0,  
  0,0,0,        //4 debug variables to DSP
  0,0,0,
  0,0,0,
  0,0,0
  
};



/* 
    This is an array of bytes, where the Knobs values will be stored. The USB handler will send
    to the GUI the value contained in this array as the knob values. 
    The GUI will shift the knobs values 15 positions to the left to show the correct values in the PC.
    These are the knobs contained in this array:    
        KNOB_1	              gua8Knobs[0]
        KNOB_2	              gua8Knobs[1]
        KNOB_3	              gua8Knobs[2]
        KNOB_4       	        gua8Knobs[3]
        KNOB_5	              gua8Knobs[4]
        KNOB_6	              gua8Knobs[5]

    This array can be accessed directly or by using the macros defined in PublicVariables.h
*/
UINT8 gua8Knobs[6]                @ RAM_START_ADDR;          //0xBD, 0xBE, 0xBF, 0xC0, 0xC1, 0xC2









/* 
    This is an array of bytes, where the Switches values will be stored. The USB handler will send
    to the GUI the value contained in this array as the switch values. 
    These are the switches contained in this array:    
        SWITCH_1	            gua8Switches[0]
        SWITCH_2              gua8Switches[1]
        FOOT_SWITCH_TAP      	gua8Switches[2]
        FOOT_SWITCH	          gua8Switches[3]
         
    This array can be accessed directly or by using the macros defined in PublicVariables.h
*/
UINT8 gua8Switches[4]             @ RAM_START_ADDR + 6;      //0xC3, 0xC4, 0xC5, 0xC6










/* 
    This is an array of bytes, where the LEDs values will be stored. The USB handler will send
    to the GUI the value contained in this array as the LEDs values. 
    The LEDs are:
        RED_LED	              gua8LEDs[0]
        GREEN_LED	            gua8LEDs[1]
    
    This array can be accessed directly or by using the macros defined in PublicVariables.h
*/
UINT8 gua8LEDs[2]                 @ RAM_START_ADDR + 10;     //0xC7, 0xC8








/* 
    This is an array of t_UINT24, where the Debug variables from the DSP will be stored. 
    The USB handler will send to the GUI the value contained in this array as the debug values from DSP    
    This array can be accessed directly or by using the macros defined in PublicVariables.h
*/
t_UINT24 gua24DebugDataFromDSP[4] @ RAM_START_ADDR + 12;     //0xC9, 0xCA, 0xCB,
                                                             //0xCC, 0xCD, 0xCE,
                                                             //0xCF, 0xD0, 0xD1
                                                             //0xD2, 0xD3, 0xD4
                                                             









/* 
    This is an array of t_UINT24, where the Debug variables to the DSP will be stored. 
    The USB handler will send to the GUI the value contained in this array as the debug values to DSP    
    This array can be accessed directly or by using the macros defined in PublicVariables.h
*/
t_UINT24 gua24DebugDataToDSP[4]   @ RAM_START_ADDR + 24;   //0xD5, 0xD6, 0xD7
                                                             //0xD8, 0xD9, 0xDA
                                                             //0xDB, 0xDC, 0xDD
                                                             //0xDE, 0xDF, 0xE0
                                                                                                                          












/* 
   gu8DSP_Flags is a byte containg flags used by the USB handler and by the user's code.      
   The following flags are bits of the gu8DSP_Flags variable:
   
       DebugDataFromPC. It's used by the USB handler to indicate that the PC has sent
                        debug data to the DSP
                        1 = Debug variables from PC has been received
                        0 = No debug variables has been received

       ReadyForBoot.    Used to know if the DSP is ready to bootload. 
                        1 = It's ok to bootload DSP
                        0 = Don't bootload DSP   

       SendDataToDSP.   This flag is used by the USB handler to indicate if data to the 
                        DSP should be sent. 
                        1 = It's ok to send data to the DSP and update any knob, switch, LED
                            or debug variable
                        0 = The PC is sending DSP code into flash. User's code should not 
                            send data to the DSP.
                        
       DataSent.        This flag is used by DSP.c to indicate if the MCU was able to read
                        or write from/to the DSP.
                        1 = Data was sent/received correctly to/from the DSP
                        0 = Data has not been sent/received
*/
u8DebugFlag gu8DSP_Flags @  (RAM_START_ADDR + 36);               //0xE1

UINT16 gua16LEDsPWM[2];














/* 
    This variable is located in flash (at the end of the segment where DSP code is stored)
    and is written by the USB handler when the MCU has received and stored correctly all the DSP code.

    If the code is not stored correctly, the variable will contain 0xFF, which is the value 
    that results from a flash erase operation.

    If the DSP code has been received correctly from the PC, the variable will have 0x00, to indicate that 
    it is ok to bootload the DSP.
*/
//const UINT8 gu8ReadyForBootloadDSP @ 0xCFFF = 0xFF;   /* Uncomment this line if DSP code is not stored in Flash */
const UINT8 gu8ReadyForBootloadDSP @ 0xCFFF = 0x00;   /* DSP code is stored in Flash. It is declared in DSPCode.c */

                