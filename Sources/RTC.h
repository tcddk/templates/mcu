/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   RTC.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro \n B13061
\version
\date
\warning None
* History:
*/

#ifndef _RTC_H
#define _RTC_H


#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"
#include "GPIO.h"
#include "BatteryLevel.h"



/* Defines */

#define RTC_TICK    RTCSC_RTIF
#define RTC_ACK()   RTCSC_RTIF = 1


/* Declarations */

extern UINT16 gu16RTC_UpdateDSPTimer;
extern UINT16 gu16RTCBattTimer;



/* Function Prototypes */

void vfnRTCSetup(void);
void interrupt vfnRTC_ISR(void);


#endif //__RTC