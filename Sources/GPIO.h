/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   GPIO.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro \n B13061
\version
\date
\warning None
* History:
*/

#ifndef _GPIO_H
#define _GPIO_H

#include <hidef.h>      /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "typedef.h"




/* Defines */

#define INPUT 0
#define OUTPUT 1

/******************JM16 PINOUT***********************/
 
//LEDS
#define LED_RED_PIN        PTFD_PTFD5  //Red LED
#define LED_GREEN_PIN      PTFD_PTFD4  //Green LED


//POTS
#define POT1_PIN      PTBD_PTBD0
#define POT2_PIN      PTBD_PTBD1
#define POT3_PIN      PTBD_PTBD2
#define POT4_PIN      PTBD_PTBD3
#define POT5_PIN      PTBD_PTBD4
#define POT6_PIN      PTBD_PTBD5

//ANALOG VOLTAGE 
#define VA_PIN        PTDD_PTDD1

//SWITCHES
#define DOCK_SW1   PTFD_PTFD6
#define DOCK_SW2   PTCD_PTCD4

#define SW_SEL    PTAD_PTAD5   //Switch Select
#define SW_LP     PTED_PTED2   //IN PULLUP1
#define SW_RP     PTAD_PTAD0   //IN PULLUP2


#define MASTER_RST  PTGD_PTGD1


//SPI COMMUNICATION
	
#define SS_MODULE   PTED_PTED7
#define SS_DOCK     PTDD_PTDD0
#define SPI_HREQ    PTGD_PTGD0       
 

           
/* Declarations */

/* Function Prototypes */

void vfnGPIOSetup(void);


#endif /* GPIO_H */