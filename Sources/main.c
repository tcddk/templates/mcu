/**************************************************************************************************
 *  Services performed by FREESCALE in this matter are performed AS IS and without any warranty.  *
 *  CUSTOMER retains the final decision relative to the total design and functionality of the     *
 *  end product. FREESCALE neither guarantees nor will be held liable by CUSTOMER for the         *
 *  success of this project. FREESCALE disclaims all warranties, express, implied or statutory    *
 *  including, but not limited to, implied warranty of merchantability or fitness for a           *
 *  particular purpose on any hardware, software ore advise supplied to the project by FREESCALE, *
 *  and or any product resulting from FREESCALE services . In no event shall FREESCALE be         *
 *  liable for incidental or consequential damages arising out of this agreement. CUSTOMER        *
 *  agrees to hold FREESCALE harmless against any and all claims demands or actions by anyone     *
 *  on account of any damage, or injury, whether commercial, contractual, or tortuous, rising     *
 *  directly or indirectly as a result of the advise or assistance supplied CUSTOMER in           *
 *  connection with product, services or goods supplied under this Agreement.                     *
 **************************************************************************************************/
 
 
/**
\mainpage
\n Copyright (c) 2008 Freescale Semiconductor
\n Freescale Confidential Proprietary
\brief
        This Project is the User's Template for the ToneCore DSP Developer Kit, 
        contains all the modules for communicating with the DSP in the 
        ToneCore Pedal, reading the value of the knobs, switches, 
        and give the clock to the DSP.

\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Diego Haro and Samuel Quiroz
\version Beta Rev A
\date    06/10/2008

*/

#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */

#include "DSP.h"
#include "GPIO.h"
#include "TPM.h"
#include "ADC.h"
#include "RTC.h"
#include "Switches.h"
#include "BatteryLevel.h"

#define DISABLE_USB()     USBCTL0=0x00      /* This macro can be used to disable the USB completely
                                               Note that if the user disables the USB, the only way to restore
                                               connectivity will be to start in bootlader mode with the pedal
                                               pressed down */
                                               
#define DSP_CLK_SETUP()    vfnTPM1Setup()      /* This macro is used to configure the TPM to be the DSP clock at 3 MHz */
#define READ_KNOBS()       vfnReadADCValues()  /* This macro is used so the user knows when the Knobs reading is been done*/

#define DSP_DATA_REFRESH_RATE		4			/* This macro is used to set the Refresh Data to DSP Rate
															The actual rate is DSP_DATA_REFRESH_RATE * 31.25us
															If PWM control of the leds is going to be used, is recommended to set this 
															to a low value, for example 8 or 4, this would give a refresh rate of 
															250us and 125us respectively */

#define LED_PWM_ENABLE			// Comment this to disable PWM control of leds

UINT8 gu8MainActualState;                   /* This variable is used to store the actual state of the main machine 
                                               It can store up to 255 different states.
                                               States are defined in the _MainStates typedef enum in the same order
                                               than the functions they represent included in the 
                                               vfnMainStateMachine[] array */

 
/** 
 * \brief   This is the state where the data is being updated to the DSP.
            The MCU sends the value of the knobs, switches, and debug vars to the DSP
            and it reads the LEDs and the debug vars from the DSP.
            The state machine which is pointer to functions: void (*const vfnMainStateMachine[])(void),
            jumps to this function when the DSP has been bootloaded and it is ready to receive and send
            data through SPI            
 * \author  Samuel Quiroz and Diego Haro 
 * \return  none
 * \warning 
 */   
void vfnMainStateUpdateDSP(void);



/** 
 * \brief   This is the state where the DSP is being bootloaded.
 * \author  Samuel Quiroz and Diego Haro 
 * \return  none
 * \warning 
 */   
void vfnMainStateBootloadDSP(void);



/* 
    You can define other states if you want. Just make a function for each state and add them
    in the _MainStates typedef enum and in the vfnMainStateMachine[] array.
    They should be in the same order so the number matches.


void vfnMyNextStateFunction(void);    //Add the function prototypes
void vfnMyOtherStateFunction(void);    

*/







/* These are possible states for the main state machine */
typedef enum
{
    MAIN_STATE_UPDATE_DSP,
    MAIN_STATE_BOOTLOAD_DSP   //  ,// <-- don't forget the comma when adding other states!
  //MY_NEXT_STATE,
  //MY_OTHER_STATE
    
  /* You can add your states here and add them in the same order into the
     vfnMainStateMachine[] array   */
}_MainStates;



/*
    This is an array of pointer to functions. It is being called in the main loop
    and will jump to function pointed by the index stored in gu8MainActualState
*/
void (*const vfnMainStateMachine[])(void) = 
{
    vfnMainStateUpdateDSP,
    vfnMainStateBootloadDSP   //, //<-- don't forget the comma when adding other states!
  //vfnMyNextStateFunction,    
  //vfnMyOtherStateFunction

  /* Remember that if you add your states here you should put them in the same order
     than in the _MainStates typedef enum
   */
};



void main(void) 
{

    //SOPT1 = 0x00;           /* Uncomment this line to disable COP */
    //DISABLE_USB();          /* Uncomment this line if you want to disable all USB connectivity */
        
    EnableInterrupts;         /* enable interrupts */
    //DisableInterrupts;      /* Uncomment this line to disable interrupts. Note that USB and RTC will not work */

    gu8MainActualState = MAIN_STATE_UPDATE_DSP;
    
    gu8DSP_Flags.DataSent = 0;
    gu8DSP_Flags.DebugDataFromPC = 1;
    gu8DSP_Flags.ReadyForBoot = 1;
    gu8DSP_Flags.SendDataToDSP = 0;
    
    vfnGPIOSetup();	          /* For pins Configuration                         */
    vfnSPISetup();            /* For MCU <--> DSP Communications Configuration  */
    vfnADCSetup();            /* For Knobs value reading                        */
    DSP_CLK_SETUP();          /* For DSP Clock Configuration                    */
    vfnRTCSetup();            /* For 1ms Timing Configuration                   */
    
    #ifdef LED_PWM_ENABLE
    
    vfnTPM2Setup();
    
    #endif
    
     
    for(;;) 
    {
        vfnMainStateMachine[gu8MainActualState]();   /* This is where the array of pointers to 
                                                        functions will jump to the state index stored
                                                        in the variable gu8MainActualState. */
        __RESET_WATCHDOG();    
    } /* loop forever */
    
}


void vfnMainStateBootloadDSP(void)
{        
        if (gu8DSP_Flags.SendDataToDSP == 0)
        {
            vfnDSP_Bootload();             /* The SendDataToDSP flag is used by DSP.c to indicate if the 
                                              bootloading process has finished.
                                              We must execute this function until the DSP has been bootloaded  */
        }
        else
        {                                  /* If the DSP has been bootloaded, wait 50 milliseconds 
                                              The variable gu8RTC_UpdateDSPTimer is being decremented by RTC.c in the
                                              RTC ISR which executes every 1 ms. */
            if (gu16RTC_UpdateDSPTimer == 0)     
            {
                gu8MainActualState = MAIN_STATE_UPDATE_DSP;  /* Change state,
                                                                MCU can now send/receive data to/from DSP */ 
                gu16RTC_UpdateDSPTimer = DSP_DATA_REFRESH_RATE;   
            }
        }               
}


void vfnMainStateUpdateDSP(void)
{    
    
    
    if (gu16RTC_UpdateDSPTimer == 0) 
    {   
        gu16RTC_UpdateDSPTimer = DSP_DATA_REFRESH_RATE;      
             
        READ_KNOBS();          /* Read ADC values and store it in the knobs vars declared in PublicVariables.c */
        ReadSwitches();       /* Read switches values and store it in the swtiches vars declared in PublicVariables.c */
        ReadBatteryLevel();   /* Read battery level and blink LED when battery is low */



        if(u8LowBatt) 
        {
              
            if(gu16RTCBattTimer == 0) 
            {
                  gu16RTCBattTimer = BATTERY_LEVEL_LED_RATE;
                  
                  #ifndef LED_PWM_ENABLE
                  
                  LED_RED_PIN = !LED_RED_PIN;
                  LED_GREEN_PIN = 0;
                  
                  #else
                  
                  LED_RED_PWM = (LED_RED_PWM == LED_PWM_PERIOD)? 0 : LED_PWM_PERIOD;
                  LED_GREEN_PWM = 0;
                  
                  #endif
            }

        } 
        else 
        {
              #ifndef LED_PWM_ENABLE
              
              LED_GREEN_PIN = GREEN_LED;        /* Set the value of the Green LED pin, based on what we have read from the DSP */
              LED_RED_PIN = RED_LED;            /* Set the value of the Red LED pin, based on what we have read from the DSP */
              
              #else
              
              LED_RED_PWM = RED_LED_PWM_VAL;
              LED_GREEN_PWM = GREEN_LED_PWM_VAL;
              
              #endif
        }


        if(gu8DSP_Flags.SendDataToDSP)
        {
            vfnStateMachineDSP();         /* If the SendDataToDSP flag is set, it means that the
                                             DSP is ready to receive and send data */
        }
    }
    /* 
        The gu8ReadyForBootloadDSP will be equal to 0x00 when the DSP code is stored correctly in the MCUs flash
        gu8DSP_Flags.ReadyForBoot is managed by the USB handler to indicate that it's ok to bootload the DSP
        The user's code can change this flag to 1 in order to bootload the MCU
    */        
    if(gu8ReadyForBootloadDSP == 0 && gu8DSP_Flags.ReadyForBoot)    
    {
        gu8MainActualState = MAIN_STATE_BOOTLOAD_DSP;   /* Change state to bootload DSP */
        gu8DSP_Flags.ReadyForBoot = 0;                  /* Clear ReadyForBoot flag so the main application 
                                                           does not start bootlader again */
    }        
}



/*

  //You can add your own states as declared previously:
  
void vfnMyNextStateFunction(void)
{
    //You can perform some actions in this state
    //and then move to any other state:

    //insert your code here

    gu8MainActualState = MY_OTHER_STATE;    //By doing this, you will change the actual state to MY_OTHER_STATE
}

void vfnMyOtherStateFunction(void)
{
    //You can perform some actions in this state
    //and then move to any other state:

    //insert your code here

    //move to the state where the DSP is being updated
    gu8MainActualState = MAIN_STATE_UPDATE_DSP;    //By doing this, you will change the actual state to MAIN_STATE_UPDATE_DSP  
}

*/
