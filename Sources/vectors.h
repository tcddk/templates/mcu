/**
Copyright (c) 2008 Freescale Semiconductor
Freescale Confidential Proprietary
\file   Vectors.h
\brief
\author Freescale Semiconductor
\author Guadalajara Applications Laboratory RTAC Americas
\author Samuel Quiroz \n B05113
\version
\date
\warning None
* History:
*/

#ifndef _Vectors_H
#define _Vectors_H


/*This USB Interrupt Service Routine is located in a Protected Flash section, that is why it is located in the
  USB_ISR_CODE section, even though there appears to be nothing in the ISR, the Bootloader part of the project
  stores in this section the Routine. 
  Please do not remove  
*/
//#pragma CODE_SEG USB_ISR_CODE
//extern void vfnUSB_ISR(void); //This Function is in Protected Bootstrap Code.
//#pragma CODE_SEG DEFAULT

#endif /* _BatteryLevel_H */