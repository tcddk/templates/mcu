/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	RTC.c
  \brief    	Driver to Configure and Read Values of the selected ADC Channels 
  \author   	Freescale Semiconductor
  \author     Diego Haro \n B13061
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    None

  * History:
  
*/

#include "RTC.h" /* include peripheral declarations */


UINT16 gu16RTC_UpdateDSPTimer = 0;
UINT16 gu16RTCBattTimer = BATTERY_LEVEL_LED_RATE;



///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   RTC Module Initialization, Interrupt every 31.25us enabled, 32kHz clk source
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  
void vfnRTCSetup(void)
{
    
    RTCMOD = 0x00;
    RTCSC = 0x58;//RTCSC_RTIE_MASK| RTCSC_RTCPS3_MASK;
}


///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   RTC Interrupt Service Routine
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/
void interrupt vfnRTC_ISR(void)
{
    RTC_ACK();
    
    if (gu16RTC_UpdateDSPTimer > 0)
    {
        gu16RTC_UpdateDSPTimer --;
    }
    
    if(u8LowBatt==1) 
    {
        if(gu16RTCBattTimer > 0)
        {        
            gu16RTCBattTimer--;
        }
    }
}
