/**
  Copyright (c) 2008 Freescale Semiconductor
  Freescale Confidential Proprietary
  \file     	GPIO.c
  \brief    	Driver to Configure GPIO's 
  \author   	Freescale Semiconductor
  \author     Diego Haro \n B13061
  \author   	Guadalajara Applications Laboratory RTAC Americas
  \version    
  \date     	
  \warning    None

  * History:
  
*/

#include "GPIO.h" 



///////////////////////////////////////////////////////////////////////////////////////
/**
 * \brief   GPIO Configuration, All inputs declared as inputs, All output declared as outputs.
            Unused pins are not declared, and are inputs by default.
 * \author  Diego Haro \n B13061
 * \return  none
 * \warning none
*/  
void vfnGPIOSetup(void)
{
     PTADD_PTADD0 = INPUT;      //SWT_PULLUP2
     PTADD_PTADD5 = OUTPUT;     //SWT_SELECT

     
     PTBDD_PTBDD0 = INPUT;      //POT1
     PTBDD_PTBDD1 = INPUT;      //POT2 
     PTBDD_PTBDD2 = INPUT;      //POT3
     PTBDD_PTBDD3 = INPUT;      //POT4
     PTBDD_PTBDD4 = INPUT;      //POT5
     PTBDD_PTBDD5 = INPUT;      //POT6

     
     //PTCDD_PTCDD0 = INPUT;
     //PTCDD_PTCDD1 = INPUT;
     //PTCDD_PTCDD2 = INPUT;
     //PTCDD_PTCDD3 = INPUT;
     PTCDD_PTCDD4 = INPUT;      //SW2
     //PTCDD_PTCDD5 = INPUT;      
     
     
     PTDDD_PTDDD0 = OUTPUT;     //SPI_SS_DOCK
     PTDDD_PTDDD1 = INPUT;      //VA_DIVIDER
     //PTDDD_PTDDD2 = INPUT;   
     //PTDDD_PTDDD7 = INPUT;


     //PTEDD_PTEDD0 = INPUT;      
     //PTEDD_PTEDD1 = INPUT;       
     PTEDD_PTEDD2 = INPUT;      //SWT_PULLUP1
     //PTEDD_PTEDD3 = INPUT;      
     PTEDD_PTEDD4 = INPUT;      //SPI_MISO
     PTEDD_PTEDD5 = OUTPUT;     //SPI_MOSI
     PTEDD_PTEDD6 = OUTPUT;     //SPI_SCK
     PTEDD_PTEDD7 = INPUT;      //SPI_SS_MODULE
     
     
     PTFDD_PTFDD0 = OUTPUT;      //MASTER_CLK
     //PTFDD_PTFDD1 = INPUT;      
     PTFDD_PTFDD4 = OUTPUT;     //LED_GREEN
     PTFDD_PTFDD5 = OUTPUT;     //LED_RED
     PTFDD_PTFDD6 = INPUT;      //SW1


     PTGDD_PTGDD0 = INPUT;      //SPI_HREQ
     PTGDD_PTGDD1 = OUTPUT;     //MASTER_RESET 
     //PTGDD_PTGDD2 = INPUT;      
     //PTGDD_PTGDD3 = INPUT;      
     
     /* ALREADY CONFIGURED BY Bootstrap code
     PTGDD_PTGDD4 = INPUT;      //XTAL
     PTGDD_PTGDD5 = INPUT;      //EXTAL
     */
}